import sys

if len(sys.argv) > 3:
	print("InputError: too many arguments\nUsage: python operations.py\nExample:\n\tpython operations.py 10 3")
	sys.exit(0)

if len(sys.argv) < 3:
	print("Usage: python operations.py\nExample:\n\tpython operations.py 10 3")
	sys.exit(0)

if not (sys.argv[1].isdigit()) or not (sys.argv[2].isdigit()):
	print("InputError: only numbers\nUsage: python operations.py\nExample:\n\tpython operations.py 10 3")
	sys.exit(0)

a = int(sys.argv[1])
b = int(sys.argv[2])

print("Sum:\t\t", a + b)
print("Difference:\t", a - b)
print("Product:\t", a * b)
if b == 0:
	print("Quotient:    \tERROR (div by zero)")
	print("Remainder:   \tERROR (modulo by zero)")
else:
	print("Quotient:\t", a / b)
	print("Remainder:\t", a % b)


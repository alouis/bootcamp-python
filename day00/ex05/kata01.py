
languages = {
    'Python': 'Guido van Rossum',
    'Ruby': 'Yukihiro Matsumoto',
    'PHP': 'Rasmus Lerdorf',
    }
for x in languages:
	#print(x, "was created by", languages[x])
	print(x, "was created by", languages.get(x))

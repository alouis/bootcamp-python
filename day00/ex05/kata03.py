import string

string = "The right format"
width = 41
fillchar = '-'

print(string.rjust(width, fillchar))

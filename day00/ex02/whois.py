import sys

num = sys.argv[1]

if len(sys.argv) < 2 or not(sys.argv[1].isdigit()):
	print("ERROR")
	sys.exit(0)

if (int(num)) == 0:
	print("I'm Zero.".format(num))
elif (int(num) % 2) == 0:  
   print("I'm Even.".format(num))  
elif (int(num) % 2) != 0:  
   print("I'm Odd".format(num))

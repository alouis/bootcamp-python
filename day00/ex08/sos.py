import sys

if len(sys.argv) == 1:
	print("ERROR")
	sys.exit(0)

	
morse = { 'A':'.-', 'B':'-...', 
          'C':'-.-.', 'D':'-..', 'E':'.', 
          'F':'..-.', 'G':'--.', 'H':'....', 
          'I':'..', 'J':'.---', 'K':'-.-',  
          'L':'.-..', 'M':'--', 'N':'-.',
          'O':'---', 'P':'.--.', 'Q':'--.-', 
          'R':'.-.', 'S':'...', 'T':'-', 
          'U':'..-', 'V':'...-', 'W':'.--', 
          'X':'-..-', 'Y':'-.--', 'Z':'--..', 
          '1':'.----', '2':'..---', '3':'...--', 
          '4':'....-', '5':'.....', '6':'-....', 
          '7':'--...', '8':'---..', '9':'----.', 
          '0':'-----', ', ':'--..--', '.':'.-.-.-', 
          '?':'..--..', '/':'-..-.', '-':'-....-', 
          '(':'-.--.', ')':'-.--.-'}

sys.argv.pop(0)

for i in range(len(sys.argv)):

	if not(sys.argv[i].isalnum() or not(sys.argv[i].isspace())):
		print("ERROR")
		sys.exit(0)

	if i > 0:
		print("/", end=' ')

	txt = sys.argv[i].upper()
	for c in txt:
		if c.isspace():
			print("/", end=' ')
		else:
			print(morse[c], end=' ')
	i += 1

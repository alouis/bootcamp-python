import sys
import string

def text_analyzer(fileobj = ""):
	M = m = pm = sp = 0

	if fileobj == "":
		fileobj = input("What is the text to analyse")
	
	for c in fileobj:
		if c.isspace():
			sp += 1
		elif c.islower():
			m += 1
		elif c.isupper():
			M += 1
		elif c in string.punctuation:
			pm += 1

	print("The text contains", M + m + sp + pm, "characters", sep=" ")
	print("-", M, "upper letters", sep = " ")
	print("-", m, "lower letters", sep = " ")
	print("-", pm, "punctuation marks", sep = " ")
	print("-", sp, "spaces", sep = " ")

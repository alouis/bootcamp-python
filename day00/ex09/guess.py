import random

rn = random.randint(0, 99)
x = 0
nb = input("This is an interactive guessing game!\nYou have to enter a number between 1 and 99 to find out the secret number.\nType 'exit' to end the game.\nGood luck!\nWhat's your guess between 1 and 99?\n>> ")

if nb == 'exit':
	exit()

elif not(nb.isdigit()):
	nb = input("That's not a number.\nWhat's your guess between 1 and 99?\n>> ")

elif nb.isdigit():
	while int(nb) != int(rn):
		x += 1
		if int(nb) > int(rn):
			nb = input("Too high!\nWhat's your guess between 1 and 99?\n>> ")
		elif int(nb) < int(rn):
			nb = input("Too low!\nWhat's your guess between 1 and 99?\n>> ")
	if int(nb) == int(rn):
		if int(rn) == 42:
			print("The answer to the ultimate question of life, the universe and everything is 42.\nCongratulations! You've got it all!")
			exit()
		if x == '0':
			print("Congratulations!\nYou got it on your first try!")
			exit()
		else:
			print("Congratulations, you've got it!\nYou won in", x, "attempts!")
			exit()

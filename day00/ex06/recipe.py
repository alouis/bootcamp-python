cookbook = { 
	'sandwich' : {
		'ingredients' : ['ham', 'bread', 'cheese', 'tomatoes'],
		'meal' : 'lunch', 
		'prep_time' : '10'
	}, 
	'cake' : {
		'ingredients' : ['flour', 'sugar', 'eggs'], 
		'meal' : 'dessert', 
		'prep_time' : '60'
	},
	'salad' : {
		'ingredients' : ['avocado', 'arugula', 'tomatoes', 'spinach'], 
		'meal' : 'lunch',
		'prep_time' : '15'
	}
}

def add_recipe():

	lst = []
	ing = '0';

	name = input("Please enter the recipe's name to be installed: ")

	while ing != '5':
		ing = input("Please enter recipe's ingredient and press enter until you're done, then press 5: ") 
		if ing != '5':
			lst.append(ing)

	meal = input("Which type of meal is it ? ")
	time = input("How long until it's ready ? ")

	cookbook.update({name : {
					'ingredients' : lst,
					'meal' : meal,
					'prep_time' : time
					}})

def del_recipe():
	d = input("Please enter the recipe's name to be deleted: ")
	cookbook.pop(d)

def print_recipe(name = ""):

	if name == "":
		name = input("Please enter the recipe's name to get its details: ")

	if name == 'sandwich':
		print("\nRecipe for sandwich:")
		print("Ingredients list:", cookbook['sandwich']['ingredients'])
		print("To be eaten for", cookbook['sandwich']['meal'], end=".\n")
		print("Takes", cookbook['sandwich']['prep_time'], "minutes of cooking.\n")

	if name == 'cake':
		print("\nRecipe for cake:")
		print("Ingredients list:", cookbook['cake']['ingredients'])
		print("To be eaten for", cookbook['cake']['meal'], end=".\n")
		print("Takes", cookbook['cake']['prep_time'], "minutes of cooking.\n")

	if name == 'salad':
		print("\nRecipe for salad:")
		print("Ingredients list:", cookbook['salad']['ingredients'])
		print("To be eaten for", cookbook['salad']['meal'], end=".\n")
		print("Takes", cookbook['salad']['prep_time'], "minutes of cooking.\n")

	else:
		print("\nRecipe for", name, ":")
		print("Ingredients list:", cookbook[name]['ingredients'])
		print("To be eaten for", cookbook[name]['meal'], end=".\n")
		print("Takes", cookbook[name]['prep_time'], "minutes of cooking.\n")

def print_cb():

	for x in cookbook:
		print("\nRecipe for", x, ":")
		print("Ingredients list:", cookbook[x]['ingredients'])
		print("To be eaten for", cookbook[x]['meal'], end=".\n")
		print("Takes", cookbook[x]['prep_time'], "minutes of cooking.\n")


for r in [1, 2, 3, 4]:

	r = input("Please select an option by typing the corresponding number:\n1: Add a recipe\n2: Delete a recipe\n3: Print a recipe\n4: Print the cookbook\n5: Quit\n>> ")

	if r == '1':
		add_recipe()
	elif r == '2':
		del_recipe()
	elif r == '3':
		print_recipe()
	elif r == '4':
		print_cb()
	elif r == '5':
		print("Cookbook closed.")
		exit()
	else:
		r = input("This option does not exist, please type the corresponding number.\nTo exit, enter 5.\n>> ")


import random
from random import shuffle

def generator(text, sep=" ", option=None):
		usr = input("Shuffle, list or ordered list ? Enter s, l or o: ")
		if usr == 's':
			lst = text.split(sep)
			random.shuffle(lst,random.random)
#			print(*lst, sep="\n")
		elif usr == 'l':
			lst = text.split(sep)
		elif usr == 'o':
			lst = text.split(sep)
			lst.sort()
		for i in lst:
			yield i

text = input(">> txt ")
if isinstance(text, str) == False:
	print(ERROR)
else:
	generator(text, sep= " ", option=None)


#CMD python
#>> from generator import generator
#>> txt = whatever
#>> for word in generator("Le Lorem Ipsum est simplement du faux texte.", sep=" "):
#>>		print(word)
#>> press enter
#>> select

import struct

class Account(object):
    ID_COUNT = 1
    def __init__(self, name, **kwargs):
        self.id = self.ID_COUNT
        self.name = name
        self.__dict__.update(kwargs)
#        if hasattr(self, 'value'):
        self.value = 0
        Account.ID_COUNT += 1
    def transfer(self, amount):
        self.value += amount	
    def __dir__(self):
        return ['id', 'name', 'value', 'amount', 'bank_id']

class Bank(object):
    def __init__(self):
        self.account = []
    def add(self, account):
        self.account.append(account)
    def transfer(self, origin, dest, amount):
		if (len(dir(origin)) / 2) != 0:
			return False
		for attribut in (dir(origin)):
			attribut.replace("__", "")
			if attribut.startwith("e") or attribut.startwith("zip") or attribut.startwith("addr"):
				return False

    def fix_account(self, account):
        """
            fix the corrupted account
            @account: int(id) or str(name) of the account
            @return         True if success, False if an error occured
		"""

andrea = Account('andrea', cat=1)
#print(andrea.__dict__)
#print(andrea.__dir__())
aerdna = Account('aerdna', cat=0)
#print(aerdna.__dict__)
#print(aerdna.__dir__())

#andrea.transfer(1000)
#print(andrea.__dict__)

at = len(andrea.__dir__())
print(at)

mybank = Bank()
mybank.add(andrea)
mybank.add(aerdna)
accounts = mybank.account
for account in accounts:
	print(account.__dict__)
	for attr, value in account.__dict__.items():
		print(attr)
		print(value)


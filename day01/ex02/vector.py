class Vector:
	def __init__(self, other):
		if type(other) == int:
			self.values = []
			item = 0.0
			self.values.append(item)
			for i in range(other-1):
				item += 1
				self.values.append(item)
		self.size = other

		if type(other) == list:
			self.values = other
			self.size = len(other)

		if type(other) == tuple:
			self.values = []
			start = other[0]
			end = other[1]
			l = 0
			while start != end:
				self.values.append(float(start))
				start += 1
				l += 1
			self.size = l

	def __add__(self, other):
		add = []
		if type(other) == int or type(other) == float:
			return (NotImplemented)
#			for i in range(self.size-1):
#				add.append(self[i] + other)
		elif isinstance(other, Vector):
			for i in range(self.size):
				add.append(self.values[i] + other.values[i])
		elif isinstance(other, Vector) == False:
			for i in range(self.size):
				add.append(self.values[i] + other[i])
			return Vector(add)

	def __radd__(self, other):
		if isinstance(self, Vector):
			add = []
			for i in range(self.size):
				add.append(self.values[i] + other[i])
			return Vector(add)
		else:
			return (NotImplemented)

	def __mul__(self, other):
		add = []
		if type(other) == int or type(other) == float:
			for i in range(self.size):
				add.append(self.values[i] * other)
		elif isinstance(other, Vector):
			for i in range(self.size):
				add.append(self.values[i] * other.values[i])
		elif isinstance(other, Vector) == False:
			for i in range(self.size):
				add.append(self.values[i] * other[i])
		return Vector(add)
		 
	def __rmul__(self, other):
		add = []
		for i in range(self.size):
			add.append(self.values[i] * other[i])
		return Vector(add)

	def __repr__(self):
		return self.values

	def __str__(self):
		return self.values

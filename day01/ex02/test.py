from vector import Vector

v1 = Vector(3)
print(v1.values)
print(v1.size)
v2 = Vector([0.0, 1.0, 2.0])
print(v2.values)
print(v2.size)
v3 = Vector((10,15))
print(v3.values)
print(v3.size,"\n")

v5 = ([3.0, 7.0, 9.0])

#v4 = v1 + v2
#print(v4.values)
#v4 = v1 * v2 
#print(v4.values)
#v4 = v1 * 2 
#print(v4.values)
#v4 = 2 * v1 
#print(v4.values)

v4 = v1 + v5
print(v4.values)
v4 = v5 + v1
print(v4.values)

v4 = v5 * v1
print(v4.values)
v4 = v1 * v5
print(v4.values)

#print(v4.__str__())
#print(v4.__repr__())

# a list of floats: Vector([0.0, 1.0, 2.0, 3.0])
# a size Vector(3) -> the vector will have values = [0.0, 1.0, 2.0]
# a range or Vector((10,15)) -> the vector will have values = [10.0, 11.0, 12.0, 13.0, 14.0]


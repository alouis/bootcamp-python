class Evaluator:
	def zip_evaluate(coefs, words):
#	def zip(*iterables):
#    sentinel = object()
#    iterators = [iter(it) for it in iterables]
#    while iterators:
#        result = []
#        for it in iterators:
#            elem = next(it, sentinel)
#            if elem is sentinel:
#                return
#            result.append(elem)
#        yield tuple(result)
		res = 0
		for c, w in zip(coefs, words):
			res += c * len(w)
		print(res)

	def enumerate_evaluate(coefs, words):
#		def enumerate(sequence, start=0):
#			n = start
#			for elem in sequence:
#				yield n, elem
#				n += 1
		for words in enumerate(words):
			print(words)

		Evaluator.zip_evaluate = staticmethod(Evaluator.zip_evaluate)
		Evaluator.enumerate_evaluate = staticmethod(Evaluator.enumerate_evaluate)

words = ["Le", "Lorem", "Ipsum", "est", "simple"]
coefs = [1.0, 2.0, 1.0, 4.0, 0.5]
Evaluator.zip_evaluate(coefs, words)
Evaluator.enumerate_evaluate(coefs, words)

import datetime

class Book:
	def __init__(self, name, lu, cd, lst):
		self.name = name
		self.last_update = lu 
		self.creation_date = cd 
		self.recipe_list = lst 

	def get_recipe_by_name(self, name):
		
#	def get_recipes_by_types(self, recipe_type):
 #   	"""Get all recipe names for a given recipe_type """
  # 		pass


	def add_recipe(self, recipe):
		self.recipe_list[recipe.recipe_type] = ({ recipe : {
								'name' : recipe.name,
								'cooking level' : recipe.cooking_level,
								'cooking time' : recipe.cooking_time,
								'ingredients' : recipe.ingredients,
								'description' : recipe.description,
								'type of recipe' : recipe.recipe_type	
									}
								})



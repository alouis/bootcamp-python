from book import Book
from recipe import Recipe
import datetime

def create_recipe():
	tpl = ("starter", "lunch", "dessert")
	name = input("Enter the name of your recipe: ")
	lvl = input("On a scale from 1 to 5 what's its level of difficulty ?\n>> ")
	tm = input("How long does it take to cook (in minutes)?\n>> ")
	ri = '0'
	lst = []
	while ri != '5':
		ri = input("Please enter recipe's ingredients one at a time and press enter until you're done, then press 5: ")
		if ri != '5':
			lst.append(ri)
	des = input("If you wish, enter here a description for your recipe, else press enter.\n>> ")
	tp = input("What type of meal is it for ? It can only be starter, lunch or dessert.\n>> ")
	if tp not in tpl:
		tp = input("Wrong input, please focus!\n>> ")

	new_rcp = Recipe(name, lvl, tm, lst, des, tp)
	return new_rcp

name = "Binary Recipes"
lu = "now" 
cd = datetime.datetime(2020, 1, 14, 11, 3, 42)
dic = ({ 'starter' : "", 'lunch' : "", 'dessert' : ""})

mybook = Book(name, lu, cd, dic)

print("Welcome in the ", mybook.name, sep="")

while True:	
	usr = input("Add your recipe to the cookbook, press enter\n>> ")
	new_rcp = create_recipe()
	mybook.add_recipe(new_rcp)

	if usr == 'quit':
		print("Your complete cookbook\n", mybook.recipe_list)

	print("Get recipe by name")
	mybook.get_recipe_by_name(new_rcp)
	
	print("Get recipe by type\nStarter:")
	type_r = 'lunch'
	if type_r in mybook.recipe_list:
		print(mybook.recipe_list['lunch'])
		

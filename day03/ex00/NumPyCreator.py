import numpy
import random

class NumPyCreator():
	def from_list(self, args):
		return numpy.array(args)
	def from_tuple(self, args):
		return numpy.array(args)
	def from_iterable(self, args):
		return numpy.array(args)
	def from_shape(self, args):
		return numpy.zeros(args)
	def random(self, args):
		return numpy.random.random_sample(args)
	def identity(self, args):
		return numpy.identity(args)

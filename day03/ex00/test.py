import numpy
from random import randint
import random

lst = [1, 2, 3]
lst1 = [4, 5, 6]
tup = (3, 5, 7, 56)
l = numpy.array([1, 2, 3])
li = numpy.int_(lst)
t = numpy.array(tup)
print('l : ', l)
print('shape : ', l.shape)
print('size : ', l.size)
print('li: ', li)
print('t :', t)

shape=(3,5)

print('\nFrom_list :\n', numpy.array([[1, 2, 3], [4, 5, 6]]))
print(numpy.array([lst, lst1]))

print('\nFrom_shape :\n', numpy.zeros((2, 3)))

print('\nRandom_npc int :\n', numpy.random.randint(1, 5, 10))
print('\nRandom_npc :\n', numpy.random.rand(3, 5))
print('\nRandom_npc :\n', numpy.random.random_sample(shape))


#test  
#>>> from NumPyCreator import NumPyCreator
#>>> npc = NumPyCreator()
#>>> npc.from_list([[1,2,3],[6,3,4]])
#array([[1, 2, 3],
#     [6, 3, 4]])
#>>> npc.from_tuple(("a", "b", "c"))
#array(['a', 'b', 'c'])
#>>> npc.from_iterable(range(5))
#array([0, 1, 2, 3, 4])
#>>> shape=(3,5)
#>>> npc.from_shape(shape)
#array([[0, 0, 0, 0, 0],
#     [0, 0, 0, 0, 0],
#     [0, 0, 0, 0, 0]])
#>>> npc.random(shape)
#array([[0.57055863, 0.23519999, 0.56209311, 0.79231567, 0.213768 ],
#     [0.39608366, 0.18632147, 0.80054602, 0.44905766, 0.81313615],
#     [0.79585328, 0.00660962, 0.92910958, 0.9905421 , 0.05244791]])
#>>> npc.identity(4)
#array([[1., 0., 0., 0.],
#     [0., 1., 0., 0.],
#     [0., 0., 1., 0.],
#     [0., 0., 0., 1.]])

import matplotlib
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
import numpy as np
from ColorFilter import ColorFilter


path = '../ex01/42AI.png'
array = mpimg.imread(path)
test = ColorFilter()
arr = test.grayscale(array)
plt.imshow(arr)
plt.show()

#array[:, :, (0)] *= 0.299
#array[:, :, (1)] *= 0.587
#array[:, :, (2)] *= 0.114

#a = array[:, :, (0)] * 0.299
#b = array[:, :, (1)] * 0.587
#c = array[:, :, (2)] * 0.114
#np.sum(array, axis=1)

#r, g, b = array[:,:,0], array[:,:,1], array[:,:,2]
#array = r * 0.299 + g * 0.587 + b * 0.114
#np.stack((array, array, array), 2)
#plt.imshow(array)
#plt.show()

#arr = 1 - np.array(array) or return 1 - array
#plt.imshow(arr)
#plt.show()

#array[np.where(array < 0.25)] = 0.25
#plt.imshow(array)
#plt.show()
#id2 = (array < 0.25)
#array[id2] = 0.25
##id5 = ((array > 0.25) & (array < 0.5))
#array[id5] = 0.5
#id7 = ((array > 0.50) & (array < 0.75))
#array[id7] = 0.75
#id1 = ((array > 0.75) & (array < 1))
#array[id7] = 1
#plt.imshow(array)
#plt.show()
#shd1 = numpy.where(a < 0.25, 0.25, a)
#shd2 = numpy.where((a > 0.25) & (a < 0.50), 0.50, a)
#shd3 = numpy.where((a > 0.50) & (a < 0.75), 0.75, a)
#shd4 = numpy.where((a > 0.75) & (a < 1), 1, a)

#array_R = mpimg.imread(path)
#array_R[:, :, (1, 2)] = 0
#array_B = mpimg.imread(path)
#array_B[:, :, (0, 2)] = 0
#array_G = mpimg.imread(path)
#array_G[:, :, (0, 2)] = 0
#plt.imshow(array_R)
#plt.show()
#plt.imshow(array_G)
#plt.show()
#plt.imshow(array_B)
#plt.show()
##
##plt.imshow(shd1)
#plt.show()
#plt.imshow(shd2)
#plt.show()
#plt.imshow(shd3)
#plt.show()
#plt.imshow(shd4)
#plt.show()

#test = ColorFilter()
#print(test.celluloid(array))
#plt.imshow(array)
#plt.show()

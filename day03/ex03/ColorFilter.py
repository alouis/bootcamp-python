import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import matplotlib.image as mpimg

class ColorFilter():
	def invert(self, array):
#		arr = 1 - np.array(array)
		return 1 - array	

	def to_blue(self, array):
		array[:, :, (0, 1)] = 0
		return array

	def to_green(self, array):
		array[:, :, (0, 2)] = 0
		return array

	def to_red(self, array):
		array[:, :, (1, 2)] = 0
		return array 

	def celluloid(self, array):
		id2 = (array < 0.25)
		array[id2] = 0.25
		id5 = ((array > 0.25) & (array < 0.5))
		array[id5] = 0.5
		id7 = ((array > 0.50) & (array < 0.75))
		array[id7] = 0.75
		id1 = ((array > 0.75) & (array < 1))
		array[id7] = 1
		return array

	def grayscale(self, array):
		r, g, b = array[:,:,0], array[:,:,1], array[:,:,2]
		array = 0.299 * r + g * 0.587 + b * 0.114
		return np.stack((array, array, array), 2)

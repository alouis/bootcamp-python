import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import matplotlib.image as mpimg

class ScrapBooker():
	def crop(self, array, dimensions, position):
		for x, y in zip(array, dimensions):
			if y > x:
				print('ERROR')
				return 'ERROR'
		cropped = array[position[0]:(position[0] + dimensions[0]), position[1]:(position[1] + dimensions[1])] 
		plt.imshow(cropped)
		plt.show()
		

	def thin(self, array, n, axis):
		if axis != 1 or axis != 0:
			print('ERROR')
			return 'ERROR'
		thined = array[::, 0:len(array):n]
		thined = array[0:len(array):n, ::]
		plt.imshow(cropped)
		plt.show()

	def juxtapose(self, array, n, axis):
		if axis != 1 or axis != 0:
			print('ERROR')
		org = array
		for i in range(n-1):
			juxt = np.concatenate([org, array], axis)
			array = juxt
		plt.imshow(juxt)
		plt.show()

	def mosaic(self, array, dimensions):
		org = array
		for i in range((dimensions[0])-1):
			juxt = np.concatenate([org, array], axis=0)
			array = juxt
		org = juxt
		for i in range((dimensions[1])-1):
			juxt = np.concatenate([org, array], axis=1)
			array = juxt
		plt.imshow(juxt)
		plt.show()


import matplotlib
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
import numpy as np

class ImageProcessor():
	def load(self, path):
		path = '42AI.png'
		img = mpimg.imread(path)
		arr = np.shape(img)
		print('Loading image of dimensions {0[0]} x {0[1]}'.format(arr))
		return img

	def display(self, array):
		plt.imshow(array)
		plt.show()

#CMM python
#>>> from ImageProcessor import ImageProcessor
#>>> imp = ImageProcessor()
#>>> arr = imp.load("../resources/42AI.png")
#>>> arr

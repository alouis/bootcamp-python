import pandas

class FileLoader():
	def load(self, path):
		df = pandas.read_csv(path, sep = '	', header = None)
		print('Loading dataset of dimensions', len(df), 'x', len(df.index))
		return df

	def display(self, df, n):
		if n > 0:	
			return df.head(n)
		else:
			n *= -1
			return df.tail(n)
	
#len(df) len(df.columns) for "[12 rows x 15 columns]" printing

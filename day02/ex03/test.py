import csv, json

class CsvReader():
	def __init__(self, filename, sep=',', header=False, skip_top=1, skip_bottom=0):
		self.filename = filename 
		self.mode = 'r'
		self.sep = sep
		self.file = None
		self.top = skip_top
		self.bottom = skip_top
		
	def __enter__(self):
		self.file = open(self.filename, self.mode)
		return self

	def __exit__(self, *args, **kwargs):
		self.file.close()

	def getdata(self):
		reader = csv.reader(self.file, delimiter= self.sep)	
		if self.top == 1:
			for i in range(self.top):
				next(reader)
			for rows in reader:
				print('--'.join(rows))
				
			
		

#txt = CsvReader('good.csv')
#txt.__enter__()
#data = txt.getdata()

if __name__ == "__main__":
	with CsvReader('good.csv') as file:
		data = file.getdata()
	

#change skip_top, add skip_bottom and use json package.
#for json see : https://hackersandslackers.com/read-write-csv-python/	
#				https://www.idiotinside.com/2015/09/18/csv-json-pretty-print-python/ 

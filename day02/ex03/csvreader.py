import csv

class CsvReader():
	def __init__(self, filename, sep=',', header=False, skip_top=0, skip_bottom=0):
		self.filename = filename
		self.mode = 'r'
		self.sep = sep
		self.header = header
		self.top = skip_top
		self.bottom = skip_bottom

	def __enter__(self):
		self.file = open(self.filename, self.mode)
		return self

	def __exit__(self, *args, **kwargs):
		self.file.close()

	def getdata(self, ):
		spamreader = csv.reader(self.file, delimiter=self.sep)
		for row in spamreader:
				print('-|-'.join(row))

	def getheader():
		pass

if __name__ == "__main__":
	with CsvReader('good.csv') as txt:
		data = txt.getdata()
 #	   header = file.getheader()

#if __name__ == "__main__":
#	with CsvReader('bad.csv') as file:
#		if file == None:
#			print("File is corrupted")

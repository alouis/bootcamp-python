from random import randint
import time

class CoffeeMachine():
	water_level = 100

	def log(func):
		def new_f(self, *args, **kwargs):
			g = open('machine.log', 'a+')
			start_time = time.time()
			result = func(self, *args, **kwargs)
			name = func.__name__.replace("_", " ")
			name = name.capitalize()
			g.write("Running: {} [--- {:.9f}]\n".format(name, time.time() - start_time))
			g.close()
			return result 
		return new_f

	@log   #start_machine = log(start_machine)
	def start_machine(self):
		if self.water_level > 20:
			return True
		else:
			print("Please add water!")
			return False
	@log
	def boil_water(self):
		return "boiling..."

	@log
	def make_coffee(self):
		if self.start_machine():
			for _ in range(20):
				time.sleep(0.1)
				self.water_level -= 1
			print(self.boil_water())
			print("Coffee is ready!")

	@log
	def add_water(self, water_level):
		time.sleep(randint(1, 5))
		self.water_level += water_level
		print("Blub blub blub...")

if __name__ == "__main__":
	machine = CoffeeMachine()
	for i in range(0, 5):
		machine.make_coffee()
	machine.make_coffee()
	machine.add_water(70)


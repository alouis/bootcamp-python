def ft_map(fct, lst):
	mapp = []
	for elem in lst:
		elem = fct(elem)
		mapp.append(elem)
	return mapp

def ft_filter(fct, lst):
	filt = []
	for elem in lst:
		if fct(elem) == True:
			filt.append(elem)
	return filt

def ft_reduce(fct, liste, initializer=None):
	lst = iter(liste)
	if initializer is None:
		value = next(lst)
	else:
		value = initializer
	for element in lst:
		value = fct(value, element)
	return value




# TEST

def mult(x):
	return (x*x)

def upp(string):
	return (string.upper())

def isalnum(string):
	for i in string:
		if i.isalnum() == False:
			return False
	return True

def iseven(nb):
	if (nb % 2) == 0:
		return True
	else:
		return False

def div(x, y):
	return (x * y)

t_lst = [0, 2, 6, 25, 11]
u_lst = ["baaaa", "glup", "mrrrh"]
w_lst = ["baaaa", "glup", "+-+", "few45few", " ", "mrrrh"]
print(ft_map(mult, t_lst))
print(ft_map(upp, u_lst))
print(ft_filter(iseven, t_lst))	
print(ft_filter(isalnum, u_lst))	
print(ft_filter(isalnum, w_lst))	
print(ft_reduce(div, t_lst))
